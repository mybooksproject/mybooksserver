package com.mybooks.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.mybooks.model.Grade;

@RepositoryRestResource(collectionResourceRel = "grade", path = "grade")
public interface GradeRepository extends MongoRepository<Grade, String> {

}
