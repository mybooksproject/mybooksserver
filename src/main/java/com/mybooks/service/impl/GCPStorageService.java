package com.mybooks.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.mybooks.model.Book;
import com.mybooks.model.Grade;
import com.mybooks.repository.GradeRepository;
import com.mybooks.service.StorageService;
import com.mybooks.utils.MyBooksUtils;

@Service
public class GCPStorageService implements StorageService {

	private final Logger logger = LoggerFactory.getLogger(GCPStorageService.class);

	private final Storage storage = StorageOptions.getDefaultInstance().getService();

	@Value("${gcp.books.bucket}")
	private String bucketName;

	@Autowired
	private GradeRepository gradeRepository;

	@Override
	public void uploadToSystem(MultipartFile file, String bookTitle, String gradeDocumentId) {

		Grade grade = gradeRepository.findById(gradeDocumentId).get();
		Book book = getBook(grade, bookTitle);

		logger.debug("Book to be updated: " + book);
		logger.debug("Uploading to GCP: " + bookTitle);

		Bucket bucket = getBucket();
		BlobId blobId = BlobId.of(bucket.getName(), book.getLocation());
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(file.getContentType()).build();

		try {
			storage.create(blobInfo, file.getBytes());
			logger.debug("Uploaded to GCP: " + bookTitle);
			gradeRepository.save(grade);
			logger.debug("Updated book: " + book);

		} catch (IOException e) {
			logger.error("Exception occurred during upload to GCP! " + bookTitle);
			throw new RuntimeException(e);
		}

	}

	private Book getBook(Grade grade, String bookTitle) {

		Book book = null;
		if (grade != null) {
			book = grade.getBooks().stream() //
					.filter(b -> b.getTitle().equalsIgnoreCase(bookTitle)) //
					.findFirst().get(); //
			if (book != null) {
				String bookLocation = MyBooksUtils.constructBookLocation(grade.getName(), book.getCategory(),
						book.getTitle());
				book.setLocation(bookLocation);
			} else {
				throw new RuntimeException();
			}
		}
		return book;
	}

	private Bucket getBucket() {
		Bucket bucket = storage.get(bucketName);
		if (bucket == null) {
			bucket = storage.create(BucketInfo.of(bucketName));
			logger.debug("Created a new bucket: " + bucketName);
		} else {
			logger.debug("Using already existing bucket: " + bucketName);
		}

		return bucket;
	}

	@Override
	public Resource downloadFromSystem(String fileName) {
		try {
			BlobId blobId = BlobId.of(getBucket().getName(), fileName);

			byte[] allBytes = storage.readAllBytes(blobId);
			String pathname = "books/" + fileName.replaceAll("/", "_");
			File file = new File(pathname);
			try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
				fileOutputStream.write(allBytes);
				Path filePath = Paths.get(file.toURI()).normalize();
				logger.debug("File to be downloaded: " + filePath);
				Resource resource = new UrlResource(filePath.toUri());
				if (resource.exists()) {
					logger.debug("Got the resource: " + fileName);
					return resource;
				} else {
					logger.error("Didn't got the resouce: " + fileName);
					Files.delete(filePath);
					throw new FileNotFoundException("File not found: " + fileName);
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(new IOException("Exception occurred while downloading file", e));
		}
	}

}
