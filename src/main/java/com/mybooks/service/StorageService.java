package com.mybooks.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

	public void uploadToSystem(MultipartFile file, String title, String documentId);

	public Resource downloadFromSystem(String title);

}
