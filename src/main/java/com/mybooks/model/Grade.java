package com.mybooks.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Document(collection = "#{modelProperties.getCollectionName()}")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Grade {

	@Id
	private String id;

	private String name;

	private List<Book> books;

	public Grade() {
	}
//
//	public Grade(String className) {
//		this.name = className;
//	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Grade [id=" + id + ", name=" + name + ", books=" + books + "]";
	}

}
