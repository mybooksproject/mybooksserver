package com.mybooks.utils;

public class MyBooksUtils {

	public static String constructBookLocation(String gradeName, String category, String bookTitle) {
		return String.format("%s/%s/%s", gradeName, category, bookTitle);
	}
}
