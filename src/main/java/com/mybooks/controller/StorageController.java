package com.mybooks.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mybooks.service.StorageService;
import com.mybooks.utils.MyBooksUtils;

@RestController
@RequestMapping("/api/v1/storage")
public class StorageController {

	private final Logger logger = LoggerFactory.getLogger(StorageController.class);

	@Autowired
	private StorageService storageService;

	@PostMapping("/upload")
	public void uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("title") String title,
			@RequestParam("documentId") String documentId) {
		logger.debug("Request made to upload file: " + title);
		storageService.uploadToSystem(file, title, documentId);
	}

	@GetMapping("/download/{grade}/{category}/{bookTitle}")
	public ResponseEntity<Resource> downloadFile(@PathVariable("grade") String gradeName,
			@PathVariable("category") String category, @PathVariable("bookTitle") String bookTitle,
			HttpServletRequest request) {
		String fileName = MyBooksUtils.constructBookLocation(gradeName, category, bookTitle);
		logger.debug("Request made to download file: " + fileName);
		Resource resource = storageService.downloadFromSystem(fileName);

		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.error("Could not determine file type.");
		}
		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		logger.debug("Sending downloaded file: " + fileName);

		return ResponseEntity.ok() //
				.contentType(MediaType.parseMediaType(contentType)) //
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + resource.getFilename() + "\"") //
				.body(resource); //
	}

}
