package com.mybooks.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybooks.model.Book;
import com.mybooks.model.Grade;
import com.mybooks.repository.GradeRepository;
import com.mybooks.service.StorageService;

@RepositoryRestController
public class GradeController {

	private final Logger logger = LoggerFactory.getLogger(GradeController.class);

	private final GradeRepository gradeRepository;

	private final StorageService storageService;

	@Autowired
	public GradeController(GradeRepository theGradeRepository, StorageService theStorageService) {
		this.gradeRepository = theGradeRepository;
		this.storageService = theStorageService;
	}

	@PostMapping("/grade/book")
	public @ResponseBody ResponseEntity<?> saveBook(@RequestParam("file") MultipartFile file,
			@RequestParam("grade") String theGrade, @RequestParam("book") String book) {

		try {
			logger.debug("GradeController.saveBook() theGrade " + theGrade);
			logger.debug("GradeController.saveBook() file " + file);
			logger.debug("GradeController.saveBook() book " + book);
			ObjectMapper mapper = new ObjectMapper();
			Grade gradeObj = mapper.readValue(theGrade, Grade.class);
			Book bookObj = mapper.readValue(book, Book.class);
			Grade grade = gradeRepository.save(gradeObj);
			storageService.uploadToSystem(file, bookObj.getTitle(), gradeObj.getId());
			Resource<Grade> resource = new Resource<Grade>(grade);
			return ResponseEntity.ok(resource);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
